const express = require("express");
const app = express();;
const PORT = Number(process.env.PORT) || 80;

app.get("/", (req, res) => {
    res.json({data: "This is the first thing"})
})

app.get("/thing2", (req, res) => {
    res.send("This is the second thing")
})

app.listen(PORT, (err) => err ? console.error(err) : console.info(`Server running on 0.0.0.0:${PORT}`))