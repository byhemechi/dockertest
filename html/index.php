<p> Thing 1: <?php echo file_get_contents("http://node/") ?></p>
<p> Thing 2: <?php echo file_get_contents("http://node/thing2") ?></p>


<h2>Fizz Buzz</h2>
<ol>
<?php for($i = 1; $i <= 100; ++$i) {?>
    <li><?php 
        $str = "";
        if($i % 3 == 0) {
            $str .= "Fizz";
        }
        if($i % 5 == 0) {
            $str .= "Buzz";
        }
        if($str == "") {
            $str = $i;
        }
        echo $str;
    ?></li>
<?php } ?>
</ol>